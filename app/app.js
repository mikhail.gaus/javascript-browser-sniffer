const http = require('http');
const url = require('url');
const fs = require('fs');
const HTMLParser = require('node-html-parser');

const hostname = '127.0.0.1';
const port = 3000;

http.createServer((req, res) => {
    let q = url.parse(req.url, true);
    let filename;
    if(q.pathname === '/') {
        filename = './public/index.html';
        fs.readFile(filename, (err, data) => {
            if (err) {
                res.writeHead(404, {'Content-Type': 'text/html'});
                return res.end("404 Not Found");
            }
            res.writeHead(200, {'Content-Type': 'text/html'});
            const root = HTMLParser.parse(data.toString());
            const reqDataDiv = root.querySelector('#data-from-request');
            reqDataDiv.set_content(buildHTMLFromRequest(req));
            res.write(root.toString());
            return res.end();
        });
    }
}).listen(port, hostname);

/**
 * Builds HTML from Node request object.
 * @param req Node request object
 */
function buildHTMLFromRequest(req) {
    let html = '';
    let headerTable = "<h3>Request Headers</h3>" +
        "<table class=\"table\">\n" +
        "  <thead>\n" +
        "    <tr>\n" +
        "      <th scope=\"col\">Header</th>\n" +
        "      <th scope=\"col\">Value</th>\n" +
        "    </tr>\n" +
        "  </thead>\n" +
        "  <tbody>\n";

    for (let [key, value] of Object.entries(req.headers)) {
        headerTable += `<tr>
            <th scope="row">${key}</th>
            <td>${value}</td>
            </tr>`;
    }

    headerTable += "</tbody>\n" +
        "</table>";

    html += headerTable;
    return html;
}
